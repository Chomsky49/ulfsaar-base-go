package http_client

import "time"

type Options struct {
	Address           string        `json:"address"`
	Timeout           time.Duration `json:"timeout"`
	DebugMode         bool          `json:"debug_mode"`
	WithProxy         bool          `json:"with_proxy"`
	ProxyAddress      string        `json:"proxy_address"`
	SkipTLS           bool          `json:"skip_tls"`
	SkipCheckRedirect bool          `json:"skip_check_redirect"`
}
