package http_client

const (
	ContentType     = "Content-Type"
	ApplicationJSON = "application/json"
	UserAgent       = "User-Agent"
	UserAgentValue  = "https://digdayatech.id"
)

const (
	startProcessingTimeKey = "start_processing_time"
	processingTimeKey      = "processing_time"
	urlKey                 = "url"
	requestKey             = "request"
	responseKey            = "response"
)
