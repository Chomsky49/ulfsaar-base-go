package middlewares

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/Chomsky49/ulfsaar-base-go/context"
	"gitlab.com/Chomsky49/ulfsaar-base-go/crypto"
	"gitlab.com/Chomsky49/ulfsaar-base-go/logger"
	"gitlab.com/Chomsky49/ulfsaar-base-go/session"
	"strings"
)

const (
	AuthorizationHeader = "authorization"
)

type (
	SessionMiddleware interface {
		AuthenticateSession(next echo.HandlerFunc) echo.HandlerFunc
	}

	impl struct {
		secret     string
		crypto     crypto.Crypto
		logger     logger.Logger
		prefixSkip []string
	}
)

func (i *impl) AuthenticateSession(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		var (
			sctx  = context.NewEmptyUlfsaarContext(ctx)
			token = ctx.Request().Header.Get(AuthorizationHeader)
		)

		if i.skipper(ctx) {
			return next(sctx)
		}

		NewSession, err := session.NewSession(i.crypto, token)
		if err != nil {
			return err
		}

		sctx.Session = NewSession
		sctx.Set("Session", NewSession)

		return next(sctx)
	}
}

func (i *impl) skipper(c echo.Context) (skip bool) {
	url := c.Request().URL.String()
	if url == "/" {
		skip = true
		return
	}

	for _, urlSkip := range i.prefixSkip {
		if strings.HasPrefix(url, urlSkip) {
			skip = true
			return
		}
	}

	return
}

func NewSessionMiddleware(secret string, crypto crypto.Crypto, logger logger.Logger, prefixSkip ...string) (SessionMiddleware, error) {
	return &impl{secret, crypto, logger, prefixSkip}, nil
}
