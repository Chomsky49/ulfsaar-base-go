package utilities

import "github.com/google/uuid"

type (
	Utils interface {
		GenerateUUID() (string, error)
	}

	utilsImpl struct {
	}
)

func NewUtils() Utils {
	return &utilsImpl{}
}

// GenerateUUID produces random ID based on UUID
func (h *utilsImpl) GenerateUUID() (string, error) {
	_uuid, err := uuid.NewRandom()
	if err != nil {
		return "", err
	}

	return _uuid.String(), nil
}
