package dbcon

import (
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

const (
	MySQL      string = "mysql"
	SQLite     string = "sqlite"
	PostgreSQL string = "postgresql"
)

type DatabaseJSONType struct {
}

func (t DatabaseJSONType) GormDBDataType(db *gorm.DB, field *schema.Field) string {
	switch db.Dialector.Name() {
	case MySQL, SQLite:
		return "JSON"
	case PostgreSQL:
		return "JSONB"
	}
	return ""
}
